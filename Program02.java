//Сортировка выбором
//бинарный поиск

import java.util.Scanner;
import java.util.Arrays;
import java.lang.Math;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int min, indexOfMin, temp;

		System.out.println("Введите длину массива");
		int l = scanner.nextInt(); 
		int [] array = new int[l];

		for (int k = 0; k < array.length; k++) {
			int a = 0;
			int b = 100;
			array[k] = a + (int) (Math.random() * b);
		}

		System.out.println("Ваш массив - " + Arrays.toString(array));

		for (int i = 0; i < array.length; i++) {
		min = array[i];
		indexOfMin = i;

		for (int j = i; j < array.length; j++) {
		if (array[j] < min) {
		min = array[j];
		indexOfMin = j;
		}
		}
		temp = array[i];
		array[i] = array[indexOfMin];
		array[indexOfMin] = temp;
	}
		System.out.println("Отсортирован - " + Arrays.toString(array));
	
		System.out.println("Введите число");
		int number = scanner.nextInt();
		boolean exists = false;

		int left = 0;
		int right = array.length - 1; 
		int middle = left + (right-left) / 2;

		while (left <= right) {
			if (number < array[middle]) {
				right = middle - 1;
			}
			else if (number > array[middle]) {
				left = middle + 1;
			} else {
				exists = true;
				break;
			}
			middle = left + (right-left) / 2;
		} 
      	if (exists) {
		    System.out.println("В массиве есть такое число");
		}
		else {
			System.out.println("В массиве нет такого числа");
		}
	}
}