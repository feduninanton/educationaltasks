// дан массив чисел 1000 шт    от 0 до 99
// определить какое число встречается больше всех и сколько раз

import java.util.Arrays;
import java.util.Random;

public class untitled {
    public static void main(String[] args) {
        Random random = new Random();

        int[] numbers = new int[1000];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(100);
        }

        System.out.println(Arrays.toString(numbers));

        int[] counts = new int[100];
        for (int i = 0; i < numbers.length; i++) {
            counts[numbers[i]]++;
        }

        for (int i = 0; i < counts.length; i++) {
            System.out.println(i + " - " + counts[i]);
        }

        int max = 0, indexOfMax = 0;
        for (int i = 0; i < counts.length; i++) {
            for (int j = i; j < counts.length; j++) {
                if (counts[j] > max) {
                    max = counts[j];
                    indexOfMax = j;
                }
            }
        }
        System.out.println("число " + indexOfMax + " встречается больше всех - " + max + " раз");


    }
}
