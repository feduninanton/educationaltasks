// дан массив из 1000 людей
// у каждого определенный возраст от 18 до 90
// определить люди какого возраста встречаются чаще других
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        User[] users = new User[1000];
        Random random = new Random();
        int min = 18, max = 91;

        for (int i = 0; i < users.length; i++){
            String name = "User" + i;
            int age = random.nextInt(max - min) + min;
            users[i] = new User(name,age);
        }

        int[] counts = new int[73];
        for (int i = 0; i < users.length; i++){
            User currentUser = users[i];
            int currentAge = currentUser.age;
            counts[currentAge - 18]++;
        }

        for(int i = 0; i < counts.length; i++) {
            System.out.println("Люди которым " + (i + 18) + " лет, встречаются " + counts[i] + " раз");
        }
            int maxAge = 0, indexOfMax = 0;
            for (int i = 0; i < counts.length; i++) {
                for (int j = i; j < counts.length; j++) {
                    if (counts[j] > maxAge) {
                        maxAge = counts[j];
                        indexOfMax = j;
                    }
                }
            }
            System.out.println("Людей с возрастом " + (indexOfMax + 18) + " встречается больше всех - " + maxAge + " раз");
        }
    }
